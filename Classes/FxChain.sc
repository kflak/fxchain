FxChain { 
    classvar <numSpeakers=2; 
    classvar <>server;

    var <fadeInTime, <level, <fadeOutTime, <>in, <>out, <>group, <>hook, <>numInputBusChannels;
    var <fadeInCurve;
    var <fadeOutCurve;
    var <fx, <bus, <gain;
    var fxGroup, gainGroup;
    var inArgPassed=false;
    var <tasks;
    var names;
    var <>lag=0.1;

    *new {|fadeInTime=1, level=1.0, fadeOutTime=1, in, out=0, group, hook, numInputBusChannels, fadeInCurve= -4, fadeOutCurve= -4|
        ^super.newCopyArgs(fadeInTime, level, fadeOutTime, in, out, group, hook, numInputBusChannels, fadeInCurve, fadeOutCurve).init;
    }

    *initClass {
        server = Server.default;
        this.prCreateFxOutSynthDef(numSpeakers);
    }

    init { 
        group = group ?? {Group.new};
        fxGroup = Group.head(group);
        gainGroup = Group.tail(group);
        server = group.server;
        bus = List.new();
        fx = IdentityDictionary.new();
        names = IdentityDictionary.new();
        if(numInputBusChannels.isNil){
            numInputBusChannels = numSpeakers;
        };
        if(in.isNil){
            bus.add(Bus.audio(server, numInputBusChannels));
            in = bus[0];
        }{
            bus.add(Bus.new(index: in, numChannels: numInputBusChannels, server: server));
            inArgPassed = true;
        };
        tasks = List.new;

        this.prCreateFxOut;
    } 

    isPlaying { ^fx[\fxOut].isPlaying; }

    *numSpeakers_ {|val| 
        this.prCreateFxOutSynthDef(val);
        numSpeakers = val;
    }

    *prCreateFxOutSynthDef{|numChannels=2|
        ServerBoot.add({
            SynthDef(\FXOut, {
                var env, lag, in, sig;
                env = EnvGen.kr(Env.asr( 
                    \attack.kr(1), 1, 
                    \release.kr(1),
                    [\fadeInCurve.kr(-4), \fadeOutCurve.kr(-4)]), 
                    gate: \gate.kr(1), 
                    doneAction: \da.kr(2)
                );
                lag = \lag.kr(0.1);
                in = In.ar(\in.kr(numChannels), numChannels);
                sig = in * \amp.kr(1, lag) * env;
                Out.ar(\out.kr(0), sig);
            }).add;
        }, server)
    }

    prSetParams {|args|
        var synth = args[0];
        var task = Array.newClear(args.size - 1); 
        args[1..].do{|i, idx|
            var param = i[0];
            var val = i[1].asStream;
            var timePattern = i[2].asStream;
            tasks.add( TaskProxy({ 
                if((val.isInteger)||(val.isFloat)){
                    synth.set(param, val);
                }{
                    timePattern = timePattern ? 1;
                    val.do({|v|
                        synth.set(param, v);
                        timePattern.next.wait;
                    });
                };
            }));
            tasks.last.play;
        }
    }

    prParseArgs {|...args|
        var synth = args[0];
        var rawArgs = args[1];
        var parsedArgs = [synth];
        rawArgs.do{|i, idx| 
            if(i.class==Symbol){
                var size, currentArray;
                parsedArgs = parsedArgs.add(Array.newClear(1));
                size = parsedArgs.size;
                currentArray = parsedArgs[size-1];
                currentArray[0] = i;
                currentArray = currentArray.add(rawArgs[idx+1]); 
                if((rawArgs[idx+2].class!=Symbol)&&(rawArgs[idx+2].isNil.not)){
                    currentArray = currentArray.add(rawArgs[idx+2]);
                }
            };
        };
        ^parsedArgs;
    }

    prMakeName {|name|
        var n;
        if(names.includesKey(name)){
            n = (name++names[name]).asSymbol;
            names[name] = names[name] + 1;
        }{
            n = name.asSymbol;
            names[name] = 1;
        }
        ^n;
    }

    add {|... args|
        var synth, p, name, n, params, inbus, outbus, parsedArgs;
        name = args[0];
        fork{
            while { this.isPlaying.not }{
                0.001.wait;
            };
            bus.add(Bus.audio(server, numSpeakers));
            inbus = bus[bus.size-2];
            outbus = bus.last;
            fx[\fxOut].set(\in, outbus);
            params = args[1];
            n = this.prMakeName(name);
            p = [ \in, inbus, \out, outbus ];
            synth = Synth.tail(fxGroup, name, p);
            fx[n] = synth;
            parsedArgs = this.prParseArgs(fx[n], params);
            this.prSetParams(parsedArgs);
        }
    }

    addPar {|... fxList|
        var inbus, outbus, synths;
        fork{
            while { this.isPlaying.not }{
                0.001.wait;
            };
            bus.add(Bus.audio(server, numSpeakers));
            inbus = bus[bus.size-2];
            outbus = bus.last;
            fx[\fxOut].set(\in, outbus);
            fxList.do({|i, idx|
                var synth;
                if (i.class==Symbol) {
                    var name = i, n, params, p, parsedArgs;
                    if (fxList[idx+1].isArray){
                        params = fxList[idx+1];
                    };
                    p = [ \in, inbus, \out, outbus] ++ params; 
                    synth = Synth.tail(fxGroup, name, p); 
                    n = this.prMakeName(name);
                    fx[n] = synth;
                    parsedArgs = this.prParseArgs(fx[n], params);
                    this.prSetParams(parsedArgs);
                }; 
            });
        }
    }

    level_ {|l|
        level = l;
        fx[\fxOut].set(\amp, level);
    }

    fadeOutTime_ {|val| 
        fadeOutTime = val;
        fx[\fxOut].set(\release, fadeOutTime);
    }

    fadeInTime_ {|val| 
        fadeInTime = val;
        fx[\fxOut].set(\attack, fadeInTime);
    }

    prCreateFxOut {
        fx[\fxOut] = Synth.new(\FXOut, [
            \attack, fadeInTime,
            \release, fadeOutTime,
            \in, bus.last,
            \amp, level,
            \fadeInCurve, fadeInCurve,
            \fadeOutCurve, fadeOutCurve,
        ], target: gainGroup, addAction: \addToTail).register;
    }

    play {}

    stop {
        tasks.do(_.stop);
        fx[\fxOut].set(\release, fadeOutTime);
        fx[\fxOut].set(\gate, 0);
    }

    free {
        fx[\fxOut].set(\release, fadeOutTime);
        fx[\fxOut].set(\gate, 0);
        SystemClock.sched(fadeOutTime, {
            tasks.do(_.stop);
            group.free;
            bus.do({|i, idx|
                if(inArgPassed.not){
                    i.free;
                }{
                    if(idx>0){
                        i.free;
                    }
                };
            });
            hook !? hook.();
        });
    } 
} 

