FxChainTest1 : UnitTest {
	test_check_classname {
		var result = FxChain.new;
		this.assert(result.class == FxChain);
	}
}


FxChainTester {
	*new {
		^super.new.init();
	}

	init {
		FxChainTest1.run;
	}
}
