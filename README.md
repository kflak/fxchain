# FxChain

### Effect Chain for SuperCollider

FxChain is a way to create aribtrarily long effect chains in SuperCollider. See help file for more details on how to use it.

### Installation

Open up SuperCollider and evaluate the following line of code:
`Quarks.install("https://gitlab.com/kflak/fxchain")`
